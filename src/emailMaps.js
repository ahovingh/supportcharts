
export default class EmailMap {
  constructor() {
    this.CSEmailMap = new Map();
    this.CSEmailMap.set('EveryWomansPlace', 'everywomansplace');
    this.CSEmailMap.set('Haven', 'haven');
    this.CSEmailMap.set('Lacasa', 'lacasa');
    this.CSEmailMap.set('Safehouse', 'safehouse');
    this.CSEmailMap.set('YWCA', 'ywca');

    this.YCEmailMap = new Map();
    this.YCEmailMap.set('Allegan', 'alllegan');
    this.YCEmailMap.set('Bay', 'baycounty');
    this.YCEmailMap.set('Berrien', 'berrien');
    this.YCEmailMap.set('Calhoun', 'calhoun');
    this.YCEmailMap.set('CTC', 'ctc');
    this.YCEmailMap.set('Eaton', 'eaton');
    this.YCEmailMap.set('Jackson', 'jackson');
    this.YCEmailMap.set('Jefferson', 'jefferson');
    this.YCEmailMap.set('Kalamazoo', 'kalcounty');
    this.YCEmailMap.set('Kent', 'kent');
    this.YCEmailMap.set('Macomb', 'macomb');
    this.YCEmailMap.set('Monroe', 'monroemi');
    this.YCEmailMap.set('Muskegon', 'muskegon');
    this.YCEmailMap.set('New Hampshire', 'nh.gov');
    this.YCEmailMap.set('Ottawa', 'ottawa');
    this.YCEmailMap.set('Roanoke', 'roanoke');
    this.YCEmailMap.set('Vanburen', 'vanburen');
    this.YCEmailMap.set('Washtenaw', 'washtenaw');

    this.YCGeneralEmailMap = new Map();
    this.YCEmailMap.forEach((value, key) => this.YCGeneralEmailMap.set(key, 'Youth Center'));

    this.clientEmailMap = new Map();
    this.clientEmailMap.set('BDO', 'bdo.com');
    this.clientEmailMap.set('TXDT', 'taxaudit.com');
    this.clientEmailMap.set('TXDT', 'taxaudit.com');
    this.clientEmailMap.set('ANDRIE', 'andrie.com');
    this.clientEmailMap.set('GAP', 'goaupair.com');
    this.clientEmailMap.set('RLND', 'rolandvirtualsonics.com');
    this.clientEmailMap.set('BSG', 'bridgespan.com');
    this.clientEmailMap.set('BTRINV', 'betterinvesting.com');
    this.clientEmailMap.set('HWL', 'bendixking.com');
    this.clientEmailMap.set('AIS', 'aisequip.com');
    this.clientEmailMap.set('GNRLCB', 'generalcable.com');
    this.clientEmailMap.set('LKADV', 'lakeshoreadvantage.com');
    this.clientEmailMap.set('IRW', 'irwinseating.com');
    this.clientEmailMap.set('LMC', 'lmc.net');
    this.clientEmailMap.set('WNJ', 'wnj.com');
    this.clientEmailMap.set('SSIDEL', 'schoolspecialty.com');
    this.clientEmailMap.set('UFCWL', 'ufclocal951.com');
    this.clientEmailMap.set('SSIDEL', 'schoolspecialty.com');
    this.clientEmailMap.set('VNRDN', 'vaneerden.com');

    this.CSGeneralEmailMap = new Map();
    this.CSEmailMap.forEach((value, key) => this.CSGeneralEmailMap.set(key, 'CaseStream'));
  }


  static getDataFromMaps(tickets, emailMaps) {
    const allTickets = [];
    emailMaps.forEach((emailMap) => {
      emailMap.forEach((value, key) => {
        let newFilteredTickets = tickets.filter(ticket => ticket.requester.email.includes(value));
        newFilteredTickets = newFilteredTickets.map((ticket) => {
          // eslint-disable-next-line
          ticket.client = key;
          return ticket;
        });
        allTickets.push(...newFilteredTickets);
      });
    });
    return allTickets;
  }
}

