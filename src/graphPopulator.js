
/* eslint-disable no-param-reassign */

import FDService from './FDService';
import EmailMap from './emailMaps';

const fdServices = new FDService();
const emailMap = new EmailMap();

const defaultColors = [
  '255,99,132',
  '54,162,235',
  '255,206,86',
  '75,192,192',
  '153,102,255',
  '255,159,64',
  '54,255,75',
  '64,192,132',
  '102,99,255',
  '162,255,54',
  '75,255,192',
];


export const clientTicketsGraph = async () => getClientData(emailMap.YCGeneralEmailMap, emailMap.CSGeneralEmailMap, emailMap.clientEmailMap);


const getClientData = async (...emailMaps) => {
  const recentTickets = await fdServices.getRecentTickets();
  const allTickets = EmailMap.getDataFromMaps(recentTickets, emailMaps);
  const groupedData = groupBy(allTickets, 'client');
  const gData = getGraphDataObject('client', 'count', groupedData, 'Cases by County');
  return gData;
};


// Call the graphing function that was passed in
const getGraphData = async graphFunction => graphFunction.call();


const getGraphDataObject = (labelProperty, dataProperty, objAr, graphName) => {
  const returnObj = {
    labels: objAr.map(val => val[labelProperty]),
    datasets: [{
      label: graphName,
      data: objAr.map(val => val[dataProperty]),
      backgroundColor: defaultColors.map(val => `rgba(${val},0.2)`),
      borderColor: defaultColors.map(val => `rgb(${val})`),
      borderWidth: 1,
    }],
  };
  return returnObj;
};


const getGraphDataWithUsers = async (ticketData) => {
  const groupedName = 'agentName';
  const agents = await fdServices.getAgents();
  // doesn't make sense not to make a parameter to reassign since making a soft copy of obj would effectively be the same thing.
  ticketData.map((obj) => {
    const assignedAgent = agents.find(agent => agent.id === obj.responder_id);
    if (assignedAgent) {
      obj[groupedName] = assignedAgent.contact.name;
    } else {
      obj[groupedName] = 'Unassigned';
    }
    return obj;
  });
  // Turn data into grouped data by agent name
  const groupedData = groupBy(ticketData, groupedName);
  const gData = getGraphDataObject(groupedName, 'count', groupedData, 'Open Cases by User');
  return gData;
};


const groupBy = (list, keyName) => {
  // Get a list of the unique keynames
  const uniqueList = Array.from(new Set(list.map(obj => obj[keyName])));
  // Gets a count of all of our items from our unique list, and returns an array of {keyname: keydata, count: count of all of the items in the list with that keydata}
  const groupedList = uniqueList.map(item => ({ [keyName]: item, count: list.filter(listObj => listObj[keyName] === item).length }));
  return groupedList;
};


export const openUsersGraph = async () => {
  const openCases = await fdServices.getAllOpenTickets();
  return getGraphDataWithUsers(openCases);
};


export const serverDownCodeGraph = async () => {
  let recentTickets = await fdServices.getRecentTickets();
  recentTickets = recentTickets.filter(ticket => ticket.subject.includes('Monitor is DOWN: '));
  recentTickets = recentTickets.map((ticket) => {
    ticket.subject = ticket.subject.replace('Monitor is DOWN: ', '');
    // gets rid of the Status code for reporting
    ticket.subject = ticket.subject.slice(0, 3);
    return ticket;
  });
  const groupedData = groupBy(recentTickets, 'subject');

  const gData = getGraphDataObject('subject', 'count', groupedData, 'Server Outage');
  return gData;
};


export const serverDownGraph = async () => {
  let recentTickets = await fdServices.getRecentTickets();
  recentTickets = recentTickets.filter(ticket => ticket.subject.includes('Monitor is DOWN: '));
  recentTickets = recentTickets.map((ticket) => {
    ticket.subject = ticket.subject.replace('Monitor is DOWN: ', '');
    // gets rid of the Status code for reporting
    ticket.subject = ticket.subject.slice(5);
    return ticket;
  });
  let groupedData = groupBy(recentTickets, 'subject');

  // sort descending
  groupedData = groupedData.sort((data1, data2) => data2.count - data1.count);
  // Only get the top five server outages for this period of 90 days.
  groupedData = groupedData.slice(0, 5);

  const gData = getGraphDataObject('subject', 'count', groupedData, 'Server Outage');
  return gData;
};


export const productTicketsGraph = async () => getClientData(emailMap.YCEmailMap, emailMap.CSEmailMap);


// If you want to know how to get graph data from ticket data, look at our sample data here
export const sampleGraph = () => {
  const gData = getGraphDataObject('label', 'value', [
    { label: 'red', value: 12 },
    { label: 'Blue', value: 19 },
    { label: 'Yellow', value: 3 },
    { label: 'Green', value: 5 },
    { label: 'Purple', value: 2 },
    { label: 'Orange', value: 3 },
  ],
  '# of Votes');
  return gData;
};


export const staleUsersGraph = async () => {
  let openCases = await fdServices.getAllOpenTickets();
  // eslint-disable-next-line
  openCases = openCases.filter((ticket) => {
    const updatedDate = new Date(ticket.updated_at);
    const currentDate = new Date();
    const staleDate = new Date();
    staleDate.setDate(currentDate.getDate() - 7);
    if (staleDate > updatedDate) {
      return ticket;
    }
  });
  return getGraphDataWithUsers(openCases);
};


export default getGraphData;

/* eslint-enable no-param-reassign */
