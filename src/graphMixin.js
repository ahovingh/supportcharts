import VueCharts from 'vue-chartjs';
import getGraphData from './graphPopulator';

export default {
  mixins: [VueCharts.mixins.reactiveProp],
  props: ['graphingFunction'],
  methods: {
    async displayChart(options) {
      const graphData = await getGraphData(this.graphingFunction);
      this.renderChart(graphData, options);
    },
  },
};
