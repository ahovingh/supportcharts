// TODO: Make settings for FD API URL and Auth Key for FD API URL
// eslint-disable-next-line no-unused-vars
export default class FDService {
  constructor() {
    this.headers = {
      'content-type': 'application/json',
      Authorization: `Basic ${btoa('iToZ3rdEUWrrAVbvAwGj')}`,
    };
    this.fdDomain = 'https://bizstream.freshdesk.com/api/v2/';
    this.recentTickets = [];
    this.agents = [];
    this.openTickets = [];
  }


  async getAgents() {
    if (this.agents.length > 0) {
      return this.agents;
    }

    this.agents = this.getData('agents');
    return this.agents;
  }


  getAllOpenTickets() {
    // Added some caching so we aren't calling the API a billion times
    if (this.openTickets.length > 0) {
      return this.openTickets;
    }

    this.openTickets = this.getData('search/tickets?query="status:2"');
    return this.openTickets;
  }


  async getData(apiURL, currentPage = 1, pageSize = 30) {
    try {
      // eslint-disable-next-line
      let requestURL = new URL(`${this.fdDomain}${apiURL}`);
      requestURL.searchParams.set('page', currentPage);
      const request = await fetch(requestURL, { headers: this.headers });

      if (request.status !== 200) {
        console.log('the request was an error', request);
      }

      let data = await request.json();

      if (data.results) {
        data = data.results;
      }

      // TODO: find out how to make this into a generator method
      // Make sure there are still more records to be grabbed
      if ((data.total > pageSize && currentPage * pageSize < data.total) || (!data.total && data.length === pageSize)) {
        const nextData = await this.getData(apiURL, currentPage + 1, pageSize);
        return nextData.concat(data);
      }

      return data;
    } catch (err) {
      console.error('there was an error obtaining the results', err);
    }

    return {};
  }

  static testmethod() {
    console.log('testing method');
  }


  getRecentTickets(daysBack = 90) {
    // Added some caching so we aren't calling the API a billion times
    if (this.recentTickets.length > 0) {
      return this.recentTickets;
    }

    // eslint-disable-next-line
    let dateSince = new Date();
    dateSince.setDate(dateSince.getDate() + (0 - daysBack));
    let dd = dateSince.getDate();
    let mm = dateSince.getMonth() + 1;

    if (mm < 10) {
      mm = `0${mm}`;
    }

    if (dd < 10) {
      dd = `0${dd}`;
    }

    const yyyy = dateSince.getFullYear();
    this.recentTickets = this.getData(`tickets?updated_since=${yyyy}-${mm}-${dd}&include=requester`);
    return this.recentTickets;
  }
}

